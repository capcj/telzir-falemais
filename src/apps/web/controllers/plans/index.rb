module Web::Controllers::Plans
  class Index
    include Web::Action
    
    expose :plansoptions

    def call(params)
      @plansoptions = PlanOptionsRepository.new.all_with_plan
    end
  end
end
