require_relative '../../../../lib/telzir/components/fale_mais_decor'

module Web::Controllers::Plans
  class Simulation
    include Web::Action
    accept :html, :json

    def call(params)
      if request.get? and params.valid?
        simulator = params.get(:simulator)
        rates_sim = []
        minutes = []
        plan_options_sim = []

        simulator.each do |v| 
          rates_sim.push({ from: '%.3d' % v[:from].to_i, to: '%.3d' % v[:to].to_i})
          plan_options_sim.push(v[:plan_option_id])
          minutes.push(v[:time])
        end

        @rates = RateRepository.new.rates_by_ddd(rates_sim)
        @plan_options = PlanOptionsRepository.new
          .all_falemais_options_by_id(plan_options_sim)

        self.status = 200
        self.body = FaleMaisDecor.new(@rates, @plan_options, minutes).simulate().to_json
      else
        status 500, "Invalid verbose, this endpoint must be accessed with GET"
      end
    end
  end
end
