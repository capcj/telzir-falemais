module Web::Controllers::Home
  class Index
    include Web::Action

    expose :rates

    def call(params)
      @rates = RateRepository.new.all
    end
  end
end
