# Plans /plans Clone Simulator Rows feature
$ document
  .on 'click', '.add-row', () ->
    parentDiv = $(this).parent() 
    clone = parentDiv.clone()
    clone.find 'input'
    .val ''
    clone.addClass 'add-line'
    rowId = $ 'form div'
    .not '.submit-row' 
    .length
    replaceId = ->
      clone.children().each -> 
        if $(this).attr('id') != undefined
          id = $(this).attr('id').replace(/-\d-|--/gi, '-' + rowId + '-')
          $(this).attr('id', id)
    replaceId()
    $ '#simulator-form div'
    .removeClass('add-line')
    $ '.add-row'
    .remove()
# Add minus/del/remove button
    minus = 
      """
        <button class='del-row' type='button'>-</button> 
      """
    $ 'form div'
    .not '.submit-row, .head, .add-line, .del-line'
    .append minus
    .addClass 'del-line'

    parentDiv.after clone 

# Remove/Del Simulator Row
$ document
  .on 'click', '.del-row', () ->
    $ this
    .parent()
    .remove()


# Simulate rates
$ '.submit-row button'
  .click ->
    data = $ 'form'
    .serializeArray()
    $.ajax '/plans/simulation',
      type: 'GET'
      dataType: 'json'
      data: data
      error: (jqXHR, textStatus, errorThrown) ->
        console.log textStatus
      success: (simulation) ->
        $ 'tr'
        .remove '.s-rate'
        simRow = (simulation) ->
          """
            <tr class='s-rate'>
              <td>#{simulation.rate.from}</td>
              <td>#{simulation.rate.to}</td>
              <td>#{simulation.time}</td>
              <td>FaleMais #{simulation.plan_option.credits}</td>
              <td class='price'>#{simulation.price_with_plan_to_s}</td>
              <td class='price'>#{simulation.price_to_s}</td>
            </tr>
          """
        appendRow = (content, element) ->
          $ content
          .appendTo element

        appendRow(simRow(simData), '#main-rates tbody') for simData in simulation
          
  
