# Configure your routes here
# See: http://hanamirb.org/guides/routing/overview/
#
# Example:
# get '/hello', to: ->(env) { [200, {}, ['Hello from Hanami!']] }
root to: 'home#index'
get '/plans', to: 'plans#index', as: :plans
get '/plans/simulation', to: 'plans#simulation', as: :plans_simulation
