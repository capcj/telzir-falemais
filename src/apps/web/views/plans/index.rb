module Web::Views::Plans
  class Index
    include Web::View

    def format_price(price) 
      '$ %.2f' % (price / 100)
    end

    def planoptions_select
      planoptionsselect = {};
      plansoptions.each { |v| planoptionsselect["#{v.plan.name} #{v.credits}"] = v.id  }
      planoptionsselect
    end
  end
end
