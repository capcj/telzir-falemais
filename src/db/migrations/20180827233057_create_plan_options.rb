Hanami::Model.migration do
  change do
    create_table :plan_options do
      primary_key :id

      foreign_key :plan_id, :plans, on_delete: :cascade, null: false

      column :credits, Integer, null: false, default: 0 # currency unit: cents
      column :surplus_rate, Integer, null: false, default: 10 # currency unit: cents
      column :created_at, DateTime, null: false
      column :updated_at, DateTime, null: false
    end

    run "INSERT INTO plan_options (plan_id, credits, surplus_rate, created_at, updated_at) VALUES 
                            (1, 30, 10, NOW(), NOW()),
                            (1, 60, 10, NOW(), NOW()),
                            (1, 120, 10, NOW(), NOW())"
  end

  down do
    drop_table :plan_options
  end
end
