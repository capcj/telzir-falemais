Hanami::Model.migration do
  change do
    create_table :rates do
      primary_key :id

      # https://stackoverflow.com/questions/42255754/phone-number-should-be-a-string-or-some-numeric-type-that-have-capacity-to-save
      # guess DDD could be just integers, but I prefer in that way to preserve my sanity
      column :from, String, null: false, size: 3
      column :to, String, null: false, size: 3
      column :price, Integer, null: false, default: 0 # currency unit: cents

      column :created_at, DateTime, null: false
      column :updated_at, DateTime, null: false

      index [:from, :to], name: :stores_correlation_index
    end

    run "INSERT INTO rates (\"from\", \"to\", price, created_at, updated_at) VALUES 
                            ('011', '016', 190, NOW(), NOW()),
                            ('016', '011', 290, NOW(), NOW()),
                            ('011', '017', 170, NOW(), NOW()),
                            ('017', '011', 270, NOW(), NOW()),
                            ('011', '018', 90, NOW(), NOW()),
                            ('018', '011', 190, NOW(), NOW())"
  end

  down do
    drop_table :rates
  end
end
