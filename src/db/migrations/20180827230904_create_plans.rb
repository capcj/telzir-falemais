Hanami::Model.migration do
  change do
    create_table :plans do
      primary_key :id
      column :name, String, null: false, size: 100

      column :created_at, DateTime, null: false
      column :updated_at, DateTime, null: false
    end

    run "INSERT INTO plans (\"name\", created_at, updated_at) VALUES 
                            ('FaleMais', NOW(), NOW())"
  end

  down do
    drop_table :plans
  end
end
