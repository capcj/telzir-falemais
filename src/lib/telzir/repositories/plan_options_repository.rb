class PlanOptionsRepository < Hanami::Repository
  associations do
    belongs_to :plan
  end

  def all_with_plan
    aggregate(:plan)
  end

  def all_falemais_options_by_id(options_list)
    options_list.map { |v| plan_options.where(id: v, plan_id: 1).map_to(PlanOptions).one }
  end

  def plan_option_by_id(id)
    plan_options
      .where(id: id, plan_id: 1)
  end

end
