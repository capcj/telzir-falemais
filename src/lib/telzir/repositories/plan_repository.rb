class PlanRepository < Hanami::Repository
  associations do
    has_many :plan_options
  end

=begin
Example:
plan = repository.create_with_options(from: "011", to: "016, price: 10000, plan_options: [{credits: 30, surplus_rate: 10}])
=end

  def create_with_options(data)
    assoc(:plan_options).create(data)
  end

  def find_with_options(id)
    aggregate(:plan_options).where(id: id).map_to(Plan).one
  end

  def by_id(id)
    aggregate(:plan_options).where(id: id).map_to(Plan).one
  end
end
