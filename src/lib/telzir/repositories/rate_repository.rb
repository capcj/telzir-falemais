class RateRepository < Hanami::Repository
  def rates_by_ddd(rates_list)
    rates_list.map { |v| rates.where(from: v[:from], to: v[:to]).map_to(Rate).one }
  end
end
