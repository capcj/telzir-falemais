class FaleMaisDecor 
  def initialize(rates, plan_options, minutes)
    @falemais = [] 
    rates.each_with_index do |v,k|
      remaining_time = minutes_to_tax(minutes[k].to_i, plan_options[k].credits.to_i) 
      price_with_plan = price_with_falemais(
        v.price.to_i,
        plan_options[k].surplus_rate.to_i,
        remaining_time.to_i
      )
      price = minutes[k].to_i * v.price.to_i
      @falemais[k] = {
        rate: v.to_h, 
        plan_option: plan_options[k].to_h, 
        time: minutes[k].to_i,
        price_with_plan: price_with_plan ,
        price: price,
        price_with_plan_to_s: format_price(price_with_plan),
        price_to_s: format_price(price) 
      }
    end
    @falemais
  end

  def minutes_to_tax(minutes, credits)
    minutes - credits
  end

  def price_with_falemais(plan_price, surplus_rate, time_spent)
    time_spent > 0 ? 
      time_spent * (plan_price + value_by_percent(plan_price, surplus_rate)) : 0
  end

  def value_by_percent(number, percentage)
    (number * percentage).div(100)
  end

  def simulate
    @falemais
  end

  def format_price(price)
    '$ %.2f' % (price / 100)
  end
end
