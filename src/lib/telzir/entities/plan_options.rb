class PlanOptions < Hanami::Entity
  attributes do
    attribute :id, Types::Int
    attribute :plan_id, Types::Int
    attribute :credits, Types::Int
    attribute :surplus_rate, Types::Int
  end
end
