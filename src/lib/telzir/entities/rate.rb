class Rate < Hanami::Entity
  attributes do
    attribute :id, Types::Int
    attribute :from, Types::String
    attribute :to, Types::String
    attribute :price, Types::Int
    attribute :created_at, Types::Time
    attribute :updated_at, Types::Time
  end
end
