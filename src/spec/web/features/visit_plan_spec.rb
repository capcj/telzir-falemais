require 'features_helper'

describe 'Visit plans' do
  it 'exposes #title' do
    visit '/plans'
    page.body.must_include("Simulação de Planos")
  end

  it 'shows the simulation form' do
    visit '/plans'
    within '#simulator-form' do
      assert page.has_css?('div', count: 3)
    end
  end

  it 'shows the simulated rates table' do
    visit '/plans'
    within '#main-rates' do
      assert page.has_css?('.s-rate', count: 1), 'Expected to find a main rates container with 1 s-rate'
    end
  end
end
