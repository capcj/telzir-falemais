require 'features_helper'

describe 'Visit home' do
  let(:repository) { RateRepository.new }
  before do
    repository.clear

    repository.create(from: '011', to: '016', price: 190)
    repository.create(from: '016', to: '011', price: 290)
    repository.create(from: '011', to: '017', price: 170)
    repository.create(from: '017', to: '011', price: 270)
    repository.create(from: '011', to: '018', price: 90)
    repository.create(from: '018', to: '011', price: 190)
  end
  it 'is sucessful' do
    visit '/'

    page.body.must_include('Conheça as nossas taxas:')
    page.body.must_include('Conheça nossos <a href="/plans">planos</a> e pague mais barato!')
  end
  it 'shows the main rates' do
    visit '/'

    within '.container' do
      assert page.has_css?('#main-rates', count: 1), 'Expected to find a main rates container'
      assert page.has_css?('#plan-mark', count: 1), 'Expected to find the plan marketing container'
    end

    within '#main-rates' do
      assert page.has_css?('.rate', count: 6), 'Expected to find 6 rates'
    end
  end
end

