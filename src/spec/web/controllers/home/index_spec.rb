require_relative '../../../spec_helper'

describe Web::Controllers::Home::Index do
  let(:action) { Web::Controllers::Home::Index.new }
  let(:params) { Hash[] }
  let(:repository) { RateRepository.new  }

  before do
    repository.clear

    @rate = repository.create(from: '011', to: '016', price: 190)
  end

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end

  it 'exposes all rates' do
    action.call(params)
    action.exposures[:rates].must_equal [@rate]
  end
end
