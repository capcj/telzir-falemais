require_relative '../../../spec_helper'

describe Web::Views::Home::Index do
  let(:exposures) { Hash[rates: []] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/home/index.html.erb') }
  let(:view)      { Web::Views::Home::Index.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #rates' do
    view.rates.must_equal exposures.fetch(:rates)
  end

  describe 'when there are no rates' do
    it 'shows a placeholder message' do
      rendered.must_include(" <p class=\"placeholder\">Não há taxas cadastradas</p>\n")
    end
  end

  describe 'when there are rates' do
    let(:rate1) { Rate.new(from: '011', to: '016', price: 190) }
    let(:rate2) { Rate.new(from: '016', to: '011', price: 290) }
    let(:rate3) { Rate.new(from: '011', to: '017', price: 170) }
    let(:rate4) { Rate.new(from: '017', to: '011', price: 270) }
    let(:rate5) { Rate.new(from: '011', to: '018', price: 90) }
    let(:rate6) { Rate.new(from: '018', to: '011', price: 190) }
    let(:exposures) { {rates: [rate1, rate2, rate3, rate4, rate5, rate6]} }

    it 'lists them all' do
      rendered.scan(/class="rate"/).count.must_equal 6
    end

    it 'hides the placeholder message' do
      rendered.wont_include(" <p class=\"placeholder\">Não há taxas cadastradas</p>\n")
    end
  end
end
