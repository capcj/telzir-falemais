require_relative '../../spec_helper'

describe Plan do
  it 'can be initialized with attributes' do
    plan = Plan.new(id: 1, name: "FaleMais")
    plan.id.must_equal 1
    plan.name.must_equal "FaleMais"
  end
end
