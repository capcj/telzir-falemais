require_relative '../../spec_helper'

describe Rate do
  it 'can be initialized with attributes' do
    rate = Rate.new(id: 1, from: '011', to: '016', price: 19000)
    rate.must_be_instance_of Rate
    rate.id.must_equal 1
    rate.from.must_equal '011'
    rate.to.must_equal '016'
    rate.price.must_equal 19000 

    rate = Rate.new(id: 2, from: '016', to: '011', price: 29000)
    rate.must_be_instance_of Rate
    rate.id.must_equal 2
    rate.from.must_equal '016'
    rate.to.must_equal '011'
    rate.price.must_equal 29000 
  end
end
