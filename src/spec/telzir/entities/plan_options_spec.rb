require_relative '../../spec_helper'

describe PlanOptions do
  it 'can be initialized with attributes' do
    # credits in minutes, surplus rates in %
    plan_options = PlanOptions.new(id: 1, plan_id: 1, credits: 30, surplus_rate: 10)
    plan_options.id.must_equal 1
    plan_options.credits.must_equal 30
    plan_options.surplus_rate.must_equal 10
  end
end
