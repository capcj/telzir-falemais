require_relative '../../spec_helper'

describe PlanRepository do
  it 'can bring a plan by their id' do
    falemais_plan = PlanRepository.new.by_id(1)  
    falemais_plan.must_be_instance_of Plan
    falemais_plan.id.must_equal 1
    falemais_plan.name.must_equal "FaleMais"
  end
end
