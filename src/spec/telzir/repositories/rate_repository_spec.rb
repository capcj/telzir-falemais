require_relative '../../spec_helper'

describe RateRepository do
  it 'can take a list of ddds and retrieve rates' do
    rates_sim = [
      { from: '011', to: '016' },
      { from: '016', to: '011' }
    ]

    rates = RateRepository.new.rates_by_ddd(rates_sim)

    rates[0].from.must_equal '011'
    rates[0].to.must_equal '016'

    rates[1].from.must_equal '016'
    rates[1].to.must_equal '011'
  end
end
