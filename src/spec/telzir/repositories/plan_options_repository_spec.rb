require_relative '../../spec_helper'

describe PlanOptionsRepository do
  it 'can bring all the falemais plan options by id' do
    falemais_options_ids = [1, 2, 3]
    plan_options = PlanOptionsRepository.new
      .all_falemais_options_by_id(falemais_options_ids)
    # FaleMais 30
    plan_options[0].must_be_instance_of PlanOptions
    plan_options[0].id.must_equal 1
    plan_options[0].plan_id.must_equal 1
    plan_options[0].credits.must_equal 30
    plan_options[0].surplus_rate.must_equal 10
    #FaleMais 60
    plan_options[1].must_be_instance_of PlanOptions
    plan_options[1].id.must_equal 2
    plan_options[1].plan_id.must_equal 1
    plan_options[1].credits.must_equal 60
    plan_options[1].surplus_rate.must_equal 10
    #FaleMais 120
    plan_options[2].must_be_instance_of PlanOptions
    plan_options[2].id.must_equal 3
    plan_options[2].plan_id.must_equal 1
    plan_options[2].credits.must_equal 120
    plan_options[2].surplus_rate.must_equal 10
  end
end
