# telzir-falemais

A basic institutional website made with [Hanami "Awesome Framework"](http://hanamirb.org) 🌸

## Testing the application

Hanami uses as main test framework [Minitest](https://github.com/seattlerb/minitest "Awesome Test Suite")

```
docker exec -it [container_id] bash
bundle exec rake test
```

## Running the application for the first time

Just

```
docker-compose up
docker exec -it [container_id] bash
```

and be happy

After the container be ready, don't need steps 2 and 3, just `docker-compose up`

## Running migrations

```
bundle exec hanami db migrate
```
