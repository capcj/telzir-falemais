FROM ruby:2.6

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -

RUN apt-get install -y build-essential nodejs

RUN gem install hanami

COPY ./entrypoint.sh /

RUN chmod 755 entrypoint.sh && chmod +x entrypoint.sh

COPY ./src /opt/telzir

WORKDIR /opt/telzir

RUN bundle install

EXPOSE 2300

ENTRYPOINT ["sh"]

CMD ["/entrypoint.sh"]
